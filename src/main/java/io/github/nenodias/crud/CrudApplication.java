package io.github.nenodias.crud;

import javax.sql.DataSource;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableAutoConfiguration
@Configuration
@SpringBootApplication
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages = "io.github.nenodias")
public class CrudApplication implements WebMvcConfigurer, ApplicationContextAware {

    public static void main(String[] args) {
        SpringApplication.run(CrudApplication.class, args);
    }

    private ApplicationContext applicationContext;

    private static final String JDBC_DIALECT = "org.hibernate.dialect.SQLiteDialect";
    private static final String JDBC_URL = "jdbc:sqlite:banco.db";
    private static final String JDBC_DRIVER = "org.sqlite.JDBC";
    private static final String JDBC_USER = "";
    private static final String JDBC_PASS = "";

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(JDBC_DRIVER);
        dataSource.setUrl(JDBC_URL);
        dataSource.setUsername(JDBC_USER);
        dataSource.setPassword(JDBC_PASS);
        return dataSource;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/static/**")
                .addResourceLocations("classpath:/static/");
    }
}
