package io.github.nenodias.crud.controller;

import io.github.nenodias.crud.model.Usuario;
import io.github.nenodias.crud.service.UsuarioService;
import java.util.List;
import javax.websocket.server.PathParam;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


//Anotação do spring para os beans do tipo controller
@Controller
//Anotação para definir a rota padrão para esse controller
@RequestMapping(value = "/usuario")
public class UsuarioController {
    
    @Autowired
    private UsuarioService usuarioService;
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody //Anotação para retornar um JSON
    public List<Usuario> index(){
        return usuarioService.buscarTodos();
    }
    
    //Url /usuario/2 irá passar o 2 na variável id
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody //Anotação para retornar um JSON
    public ResponseEntity<Usuario> show(@PathVariable(value = "id") Long id){
        Usuario usuario = usuarioService.buscarPorId(id);
        ResponseEntity<Usuario> response = null;
        if(usuario != null && usuario.getId() != null){
            Hibernate.initialize(usuario);
            response = ResponseEntity.ok().body(usuario);
        }else{
            response = ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return response;
    }
    
    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseBody //Anotação para retornar um JSON
    public ResponseEntity<Usuario> save( @RequestBody Usuario usuario){
        usuarioService.salvar(usuario);
        return ResponseEntity.status(HttpStatus.CREATED).body(usuario);
    }
    
    //Url /usuario/2 irá passar o 2 na variável id
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody //Anotação para retornar um JSON
    public ResponseEntity<Void> delete(@PathVariable(value = "id") Long id){
        Usuario usuario = usuarioService.buscarPorId(id);
        if(usuario != null){
            usuarioService.excluir(usuario);
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
