package io.github.nenodias.crud.dao;

import io.github.nenodias.crud.model.Usuario;
import java.util.List;

public interface UsuarioDao {
    
    Usuario buscarPorId(Long id);
    
    List<Usuario> buscarPorNome(String nome);
    
    void salvar(Usuario usuario);
    
    void excluir(Usuario usuario);

    List<Usuario> buscarTodos();
    
}
