package io.github.nenodias.crud.dao.impl;

import io.github.nenodias.crud.dao.UsuarioDao;
import io.github.nenodias.crud.model.Usuario;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

//Implementação dos daos do spring precisa dessa anotação
@Repository
//Annotation para abrir a transação com o banco de dados
@Transactional
public class UsuarioDaoImpl implements UsuarioDao{

    @Autowired
    private EntityManager entityManager;
    
    //Método que pega a sessão atual do hibernate
    private Session getCurrentSession(){
        Session session = entityManager.unwrap(org.hibernate.Session.class);
        return session;
    }
	

    @Override
    public Usuario buscarPorId(Long id) {
        return (Usuario) getCurrentSession().get(Usuario.class, id);
    }

    @Override
    public List<Usuario> buscarPorNome(String nome) {
        return getCurrentSession().createCriteria(Usuario.class, "t").add(
                //Buscar todos os usuario com o nome que contém a string passada, ignorando letras maiúsculas e minúsculas
                Restrictions.like("t.nome", nome, MatchMode.ANYWHERE).ignoreCase()
        ).list();
    }

    @Override
    public void salvar(Usuario usuario) {
        getCurrentSession().saveOrUpdate(usuario);
    }

    @Override
    public void excluir(Usuario usuario) {
        getCurrentSession().delete(usuario);
    }

    @Override
    public List<Usuario> buscarTodos() {
        return getCurrentSession().createCriteria(Usuario.class).list();
    }
    
}
