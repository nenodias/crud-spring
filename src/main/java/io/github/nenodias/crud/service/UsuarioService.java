package io.github.nenodias.crud.service;

import io.github.nenodias.crud.model.Usuario;
import java.util.List;

public interface UsuarioService {
    List<Usuario> buscarTodos();
    Usuario buscarPorId(Long id);
    void salvar(Usuario usuario);
    void excluir(Usuario usuario);
}
