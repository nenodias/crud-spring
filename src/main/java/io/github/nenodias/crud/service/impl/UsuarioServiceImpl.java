package io.github.nenodias.crud.service.impl;

import io.github.nenodias.crud.dao.UsuarioDao;
import io.github.nenodias.crud.model.Usuario;
import io.github.nenodias.crud.service.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//Anotação do spring para os services que são os beans com as regras de negócio
@Service
public class UsuarioServiceImpl implements UsuarioService {
    
    @Autowired
    private UsuarioDao usuarioDao;

    @Override
    public List<Usuario> buscarTodos() {
        return usuarioDao.buscarTodos();
    }
    
    @Override
    public Usuario buscarPorId(Long id){
        return usuarioDao.buscarPorId(id);
    }

    @Override
    public void salvar(Usuario usuario) {
        usuarioDao.salvar(usuario);
    }

    @Override
    public void excluir(Usuario usuario) {
        usuarioDao.excluir(usuario);
    }
    
}
