Vue.config.devtools = true;
axios = axios.create({
    headers:{
        'Accept':'application/json',
        'Content-type':'application/json'
    }
})
require(['/static/js/app/App.js'], function(App){
    
    const vm = new Vue({
        el: '#app',
        render:(h)=>h(App)
    });
});