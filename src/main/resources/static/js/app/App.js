define(function(require, exports, module) {
    const Topo = require('./Topo.js');
    const Cadastro = require('./Usuarios/index.js');
    
    const TEMPLATE = `
        <div style="display:flex;">
            <my-topo></my-topo>
            <div style="margin-top:60px;" class="container is-fluid">
                <div class="notification">
                    <Cadastro></Cadastro>
                </div>
            </div>
            
        </div>
    `;
    
    const App = Vue.component('my-app',{
        name:'App',
        data(){
            return {
                message:'Olá mundo'
            };
        },
        template:TEMPLATE
    });

    module.exports = App;
});