define(function(require, exports, module) {
    
    const TEMPLATE = `
        <nav class="navbar is-fixed-top has-shadow">
            <div class="navbar-brand">
              <a class="navbar-item" href="#">
                CRU7
              </a>
              <div class="navbar-burger burger" data-target="navbarExample">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>

            <div id="navbarExample" class="navbar-menu">
              <div class="navbar-start">
                <a class="navbar-item" href="#">
                  Home
                </a>
              </div>


            </div>
        </nav>
    `;
    
    const Topo = Vue.component('my-topo',{
        name:'Topo',
        template:TEMPLATE

    });

    module.exports = Topo;
});
    