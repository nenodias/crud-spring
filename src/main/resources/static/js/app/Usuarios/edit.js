define(function(require, exports, module) {
    const TEMPLATE = `
        <div>
            <label>
                Nome:<input type="text" v-model="registro.nome" class="input" />
            </label>
            <label>
                Email:<input type="text" v-model="registro.email" class="input" />
            </label>
            <label>
                Login:<input type="text" v-model="registro.login" class="input" />
            </label>
            <label class="checkbox">
              <input type="checkbox" v-model="registro.ativo">
              Ativo
            </label>
            <br/>
            <br/>
            <button type="button" @click="salvar" class="button is-success">Salvar</button>
            <button type="button" @click="cancelar" class="button is-danger">Cancelar</button>
        </div>
    `;
    
    const fnNovoRegistro = () => {
        return {
            id:null,
            nome:'',
            email:'',
            login:'',
            ativo:false
        }
    };
    
    const Edit = Vue.component('Edit',{
        name:'Edit',
        data(){
            return{
                registro:fnNovoRegistro()
            };
        },
        methods:{
            salvar(){
                this.$emit('salvar', this.registro);
                this.registro = fnNovoRegistro();
            },
            cancelar(){
                this.registro = fnNovoRegistro();
            },
            setRegistro(registro){
                this.registro = Object.assign({}, registro);
            }
        },
        template:TEMPLATE
    });
    module.exports = Edit;
});