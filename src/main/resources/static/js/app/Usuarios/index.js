define(function(require, exports, module) {
    const Edit = require('./edit.js');
    const TEMPLATE = `
        <div>
            <table class="table is-bordered is-striped is-narrow is-hoverable">
                <thead>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Login</th>
                    <th>Ativo</th>
                    <th></th>
                </thead>
                <tbody>
                    <tr v-for="(item, key) in toList(registros)" :key="key">
                        <td>{{item.id}}</td>
                        <td>{{item.nome}}</td>
                        <td>{{item.email}}</td>
                        <td>{{item.login}}</td>
                        <td>{{item.ativo ? 'Sim' : 'Não'}}</td>
                        <td>
                            <button @click="editarRegistro(item)" type="button" class="button is-warning">Editar</button>
                            <button @click="excluirRegistro(item)" type="button" class="button is-danger">Excluir</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <Edit ref="edit" @salvar="salvarRegistro"></Edit>
        </div>
    `;
    const Cadastro = Vue.component('Cadastro',{
        name:'Cadastro',
        data(){
            return {
                registros:{
                    
                }
            }
        },
        methods:{
            toList(objeto){
                return Object.keys(objeto).reduce((sum, idx) =>{
                    sum.push(objeto[idx]);
                    return sum;
                },[]);
            },
            editarRegistro(registro){
                this.$refs.edit.setRegistro(registro);
            },
            salvarRegistro(registro){
                console.log('Salvando');
                console.log(registro);
                axios({
                    url:'/usuario/',
                    method:'post',
                    data:registro
                }).then(resp =>{
                    const dado = resp.data;
                    if(dado.id){
                        this.registros[dado.id] = dado;
                        this.$forceUpdate();
                    }
                }).catch(err=>console.error(err));
            },
            excluirRegistro(registro){
                console.log('Excluindo');
                console.log(registro);
                axios({
                    url:`/usuario/${registro.id}`,
                    method:'delete',
                }).then(resp =>{
                    if(resp.status !== 404){
                        delete this.registros[registro.id];
                        this.$forceUpdate();
                    }
                }).catch(err=>console.error(err));
            }
        },
        created(){
            axios('/usuario/').then(resp =>{
               const dados = resp.data;
               if(dados && dados.length > 0){
                dados.reduce((sum, item)=>{
                    if(item && item.id){
                     this.registros[item.id] = item;
                    }
                });
               }
               this.$forceUpdate();
            }).catch(err=>console.error(err));
        },
        template:TEMPLATE
    });
    module.exports = Cadastro;
});